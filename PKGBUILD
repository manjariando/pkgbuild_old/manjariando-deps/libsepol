# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Nicolas Iooss (nicolas <dot> iooss <at> m4x <dot> org)
# Contributor: Timothée Ravier <tim@siosm.fr>
# Contributor: Nicky726 (Nicky726 <at> gmail <dot> com)
# Contributor: Sergej Pupykin (pupykin <dot> s+arch <at> gmail <dot> com)
#
# This PKGBUILD is maintained on https://github.com/archlinuxhardened/selinux.
# If you want to help keep it up to date, please open a Pull Request there.

pkgname=libsepol
_pkgver=3.4
pkgver=${_pkgver/-/}
pkgrel=1.4
pkgdesc="SELinux binary policy manipulation library"
arch=('x86_64' 'aarch64') # 'i686' 'armv6h')
url='http://userspace.selinuxproject.org'
license=('LGPL2.1')
groups=('selinux')
makedepends=('flex')
options=('staticlibs')
conflicts=("selinux-usr-${pkgname}" "${pkgname}")
provides=("selinux-usr-${pkgname}=${pkgver}-${pkgrel}")
source=("https://github.com/SELinuxProject/selinux/releases/download/${_pkgver}/${pkgname}-${_pkgver}.tar.gz")
sha256sums=('fc277ac5b52d59d2cd81eec8b1cccd450301d8b54d9dd48a993aea0577cf0336')

build() {
    cd "${pkgname}-${_pkgver}"
    export CFLAGS="${CFLAGS} -fno-semantic-interposition"
    make
}

package() {
    depends=('glibc')

    cd "${pkgname}-${_pkgver}"
    make DESTDIR="${pkgdir}" SHLIBDIR=/usr/lib install
}
